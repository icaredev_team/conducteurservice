package com.icare.conducteur_service.dao;

import com.icare.conducteur_service.model.Conducteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ahadri on 1/20/17.
 */

@Repository
public interface ConducteurRepository extends JpaRepository<Conducteur, Long>{
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Conducteur conducteur set conducteur.nom = :nom, conducteur.prenom = :prenom, conducteur.telephone = :telephone, conducteur.email = :email, conducteur.cin = :cin, conducteur.age = :age where conducteur.id = :id")
	void updateUser(@Param("nom") String nom, @Param("prenom") String prenom, @Param("telephone") String telephone, @Param("email") String email, @Param("cin") String cin, @Param("age") String age, @Param("id") Long id);
}
