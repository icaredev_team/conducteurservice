package com.icare.conducteur_service.dao;

import com.icare.conducteur_service.model.Conducteur;
import com.icare.conducteur_service.model.Voiture;
import com.icare.conducteur_service.wrapper.VoitureWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ahadri on 1/20/17.
 */
@Repository
public class VoitureRepository {

	@Autowired
	private ConducteurRepository conducteurRepository;

	public VoitureRepository() {
	}

	public void addVoiture(VoitureWrapper voitureWrapper) {
		Voiture voiture = voitureWrapper.getVoiture();
		Long conducteurId = voitureWrapper.getId();
		Conducteur conducteur = conducteurRepository.findOne(conducteurId);
		conducteur.setVoiture(voiture);
		conducteurRepository.save(conducteur);
	}
}
