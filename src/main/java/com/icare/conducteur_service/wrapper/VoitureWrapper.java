package com.icare.conducteur_service.wrapper;

import com.icare.conducteur_service.model.Voiture;

/**
 * Created by ahadri on 1/20/17.
 */
public class VoitureWrapper {

	private Long id;
	private Voiture voiture;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Voiture getVoiture() {
		return voiture;
	}

	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}

	public VoitureWrapper() {
	}

	public VoitureWrapper(Long id, Voiture voiture) {
		this.id = id;
		this.voiture = voiture;
	}

	@Override
	public String toString() {
		return "VoitureWrapper{" +
					   "id=" + id +
					   ", voiture=" + voiture.toString() +
					   '}';
	}
}
