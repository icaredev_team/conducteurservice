package com.icare.conducteur_service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by ahadri on 1/20/17.
 */
@Entity
public class Voiture {


	@Id
	@GeneratedValue
	private Long id;

	@OneToOne(mappedBy = "voiture")
	@JsonIgnore
	private Conducteur conducteur;

	private String matricule;
	private String couleur;
	private String modele;

	@Override
	public String toString() {
		return "Voiture{" +
					   "id=" + id +
					   ", conducteur=" + conducteur +
					   ", matricule='" + matricule + '\'' +
					   ", couleur='" + couleur + '\'' +
					   ", modele='" + modele + '\'' +
					   '}';
	}

	public Voiture() {
	}

	public Voiture(String matricule, String couleur, String modele) {
		this.matricule = matricule;
		this.couleur = couleur;
		this.modele = modele;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Conducteur getConducteur() {
		return conducteur;
	}

	public void setConducteur(Conducteur conducteur) {
		this.conducteur = conducteur;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}
}
