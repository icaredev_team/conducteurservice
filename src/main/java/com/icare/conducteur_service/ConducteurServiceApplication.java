package com.icare.conducteur_service;


import com.icare.conducteur_service.dao.ConducteurRepository;
import com.icare.conducteur_service.dao.VoitureRepository;
import com.icare.conducteur_service.model.Conducteur;
import com.icare.conducteur_service.model.Voiture;
import com.icare.conducteur_service.wrapper.VoitureWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SpringBootApplication
@RestController
@RequestMapping("/conducteur-service")
@EnableBinding(MessageStream.class)
@EnableDiscoveryClient
public class ConducteurServiceApplication {

	@Autowired
	private ConducteurRepository conducteurRepository;
	@Autowired
	private VoitureRepository voitureRepository;

	@Autowired
	MessageStream messageStream;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String howAmI() {
		return "conducteur-service";
	}


	@StreamListener(MessageStream.ADD_CONDUCTEUR_INPUT)
	@SendTo(MessageStream.API_GATE_WAY_OUTPUT)
	public Conducteur addConducteur(Conducteur conducteur)
	{
		System.out.println("=---->" + conducteur.toString());
		return conducteurRepository.save(conducteur);
	}


	@StreamListener(MessageStream.UPDATE_CONDUCTEUR_INPUT)
	@SendTo(MessageStream.API_GATE_WAY_OUTPUT)
	public String updateConducteur(Conducteur conducteur) {
		System.out.println("===> " + conducteur.toString());
		Conducteur conducteur1 = conducteurRepository.findOne(conducteur.getId());
		if ( conducteur1 != null )
		{
			Voiture voiture = new Voiture();
			System.out.println("))))>" + voiture.toString());
			voiture.setCouleur(conducteur1.getVoiture().getCouleur());
			voiture.setModele(conducteur1.getVoiture().getModele());
			voiture.setMatricule(conducteur1.getVoiture().getMatricule());
			conducteur1.setVoiture(voiture);
			System.out.println("____>" + conducteur1);
			conducteurRepository.updateUser(conducteur.getNom(), conducteur.getPrenom(), conducteur.getTelephone(), conducteur.getEmail(), conducteur.getCin(), conducteur.getAge(), conducteur.getId());
			return "";
		}
		return "Conducteur not found";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/conducteurs/{conducteurId}")
	public Conducteur getConducteur(@PathVariable("conducteurId") Long id)
	{
		if (conducteurRepository.findOne(id) != null )
			return conducteurRepository.findOne(id);
		return null;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/conducteurs")
	public List<Conducteur> getClients() {
		return conducteurRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/conducteurs/{conducteurId}")

	public void dropConducteur(@PathVariable("conducteurId") Long id) {
		conducteurRepository.delete(id);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/voitures")
	public void addVoiture(@RequestBody VoitureWrapper voitureWrapper) {
		voitureRepository.addVoiture(voitureWrapper);
	}


	public static void main(String[] args) {
		SpringApplication.run(ConducteurServiceApplication.class, args);
	}
}

 interface MessageStream{
	 String UPDATE_CONDUCTEUR_INPUT = "updateConducteurInput";
	 String ADD_CONDUCTEUR_INPUT = "addConducteurInput";
	 String API_GATE_WAY_OUTPUT = "apiGateWayOutput";


	 @Input(UPDATE_CONDUCTEUR_INPUT)
	SubscribableChannel updateConducteurInput();
	 @Input(ADD_CONDUCTEUR_INPUT)
	 SubscribableChannel addConducteurInput();

	@Output(API_GATE_WAY_OUTPUT)
	SubscribableChannel apiGateWayOutput();


}